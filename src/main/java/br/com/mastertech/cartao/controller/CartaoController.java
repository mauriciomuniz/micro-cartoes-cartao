package br.com.mastertech.cartao.controller;

import br.com.mastertech.cartao.DTO.CartaoDTO;
import br.com.mastertech.cartao.DTO.CartaoGetDTO;
import br.com.mastertech.cartao.client.ClienteCartao;
import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.model.Cliente;
import br.com.mastertech.cartao.model.Status;
import br.com.mastertech.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoDTO cadastrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO) {
            CartaoDTO retornoCartaoDTO = cartaoService.cadastrarCartao(cartaoDTO);
            return retornoCartaoDTO;
    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public CartaoDTO ativarCartao (@PathVariable(name = "numero") int numero, @RequestBody CartaoDTO cartaoDTO) {
        return cartaoService.ativarCartao(numero, cartaoDTO);
    }

    @GetMapping("/{numero_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public CartaoGetDTO buscarCartaoPeloSeuNumero(@PathVariable(name = "numero_cartao") int numero) {
            CartaoGetDTO cartaoGetDTO = cartaoService.buscarCartaoPorNumero(numero);
            return cartaoGetDTO;
    }

    @GetMapping("/id/{id_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public Cartao buscarCartaoPorID(@PathVariable(name = "id_cartao") int idCartao) {
            Cartao cartao = cartaoService.buscarCartaoPorID(idCartao);
            return cartao;
    }

    @PatchMapping("/{cartao_id}/expirar")
    @ResponseStatus(HttpStatus.OK)
    public Status inativarCartao (@PathVariable(name = "cartao_id") int idCartao) {
        return cartaoService.inativarCartao(idCartao);
    }
}
