package br.com.mastertech.cartao.repository;

import br.com.mastertech.cartao.model.Cartao;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Cartao findByNumero(int numeroCartao);

    boolean existsByIdAndClienteId(int cartaoId, int clienteId);
}
