package br.com.mastertech.cartao.client;

import br.com.mastertech.cartao.model.Cliente;
import com.netflix.client.ClientException;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClientCardLoadBalancerFallback implements ClienteCartao {

    private Exception exception;

    public ClientCardLoadBalancerFallback(Exception exception) {
        this.exception = exception;
    }

    @Override
    public Cliente buscarClientePorID(int id) {
        if (exception.getCause() instanceof ClientException) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "API cliente indisponível");
        }
        throw (RuntimeException) exception;
    }

}
