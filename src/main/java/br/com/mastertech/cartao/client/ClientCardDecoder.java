package br.com.mastertech.cartao.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class ClientCardDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 400) {
            return new ClientNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }

}
