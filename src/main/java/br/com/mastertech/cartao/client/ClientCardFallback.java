package br.com.mastertech.cartao.client;

import br.com.mastertech.cartao.model.Cliente;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ClientCardFallback implements ClienteCartao{

    @Override
    public Cliente buscarClientePorID(int id) {
      throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "API Cliente indisponível");
    }

}
