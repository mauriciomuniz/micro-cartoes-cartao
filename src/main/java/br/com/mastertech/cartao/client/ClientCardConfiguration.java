package br.com.mastertech.cartao.client;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClientCardConfiguration {

    @Bean
    public ErrorDecoder getClientCardDecoder() {
        return new ClientCardDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallback(new ClientCardFallback(), RetryableException.class)
                .withFallbackFactory(ClientCardLoadBalancerFallback::new, RuntimeException.class)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
