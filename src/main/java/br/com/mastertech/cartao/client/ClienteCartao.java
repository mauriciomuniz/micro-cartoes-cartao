package br.com.mastertech.cartao.client;

import br.com.mastertech.cartao.model.Cliente;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "CLIENTE", configuration = ClientCardConfiguration.class)
public interface ClienteCartao {

    @GetMapping("/clientes/{id}")
    Cliente buscarClientePorID(@PathVariable(name = "id") int id);
}