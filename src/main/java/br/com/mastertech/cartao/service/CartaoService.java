package br.com.mastertech.cartao.service;


import br.com.mastertech.cartao.DTO.CartaoDTO;
import br.com.mastertech.cartao.DTO.CartaoGetDTO;
import br.com.mastertech.cartao.client.CardNotFoundException;
import br.com.mastertech.cartao.client.ClienteCartao;
import br.com.mastertech.cartao.model.Cartao;
import br.com.mastertech.cartao.model.Cliente;
import br.com.mastertech.cartao.model.Status;
import br.com.mastertech.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteCartao clienteCartao;

    public CartaoDTO cadastrarCartao(CartaoDTO cartaoDTO) {

            Cliente cliente = clienteCartao.buscarClientePorID(cartaoDTO.getClienteId());

            Cartao cartao = new Cartao();
            CartaoDTO saidaCartaoDTO;
            int numeroCartao = Integer.parseInt(cartaoDTO.getNumero());

            cartao.setNumero(numeroCartao);
            cartao.setAtivo(false);
            cartao.setCliente(cliente);
            cartao = cartaoRepository.save(cartao);

            saidaCartaoDTO = montaDTO(cartao);
            return saidaCartaoDTO;
    }

    public CartaoDTO ativarCartao(int numero, CartaoDTO cartaoDTO) {
        Cartao cartao;
        CartaoGetDTO retornoCartao;
        CartaoDTO saidaCartaoDTO;

        retornoCartao = buscarCartaoPorNumero(numero);

        cartao = atualizarCartao(retornoCartao, cartaoDTO.isAtivo());

        saidaCartaoDTO = montaDTO(cartao);
        return saidaCartaoDTO;
    }

    public Status inativarCartao(int idCartao) {
        Status status = new Status();

        Cartao cartao = buscarCartaoPorID(idCartao);
        if (cartao.isAtivo()) {
            cartao.setAtivo(false);
            cartaoRepository.save(cartao);
        }
        status.setStatus("ok");

        return status;
    }

    public CartaoGetDTO buscarCartaoPorNumero(int numero) {
        CartaoGetDTO cartaoGetDTO = new CartaoGetDTO();
        try {
            Cartao cartao = cartaoRepository.findByNumero(numero);
            cartaoGetDTO.setId(cartao.getId());
            cartaoGetDTO.setClienteId(cartao.getCliente().getId());
            cartaoGetDTO.setNumero(String.valueOf(cartao.getNumero()));
            return cartaoGetDTO;
        } catch (RuntimeException e) {
            throw new CardNotFoundException();
        }
    }

    public Cartao atualizarCartao(CartaoGetDTO cartaoGetDTO, boolean ativo) {
        Cliente cliente = new Cliente();
        Cartao cartao = new Cartao();

        cliente.setId(cartaoGetDTO.getClienteId());
        cartao.setCliente(cliente);
        cartao.setId(cartaoGetDTO.getId());
        cartao.setNumero(Integer.parseInt(cartaoGetDTO.getNumero()));
        cartao.setAtivo(ativo);

        return cartaoRepository.save(cartao);
    }

    public CartaoDTO montaDTO(Cartao cartao) {
        CartaoDTO cartaoDTO = new CartaoDTO();

        cartaoDTO.setNumero(String.valueOf(cartao.getNumero()));
        cartaoDTO.setClienteId(cartao.getCliente().getId());
        cartaoDTO.setId(cartao.getId());
        cartaoDTO.setAtivo(cartao.isAtivo());

        return cartaoDTO;
    }

    public Cartao buscarCartaoPorID(int idCartao) {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(idCartao);
        if (cartaoOptional.isPresent()) {
            Cartao cartao = cartaoOptional.get();
            return cartao;
        } else {
            throw new CardNotFoundException();
        }
    }

    public Cartao salvarCartao(Cartao cartao) {
        return cartaoRepository.save(cartao);
    }
}